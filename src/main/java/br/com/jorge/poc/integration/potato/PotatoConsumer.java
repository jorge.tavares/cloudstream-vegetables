package br.com.jorge.poc.integration.potato;

import br.com.jorge.poc.potato.Potato;
import br.com.jorge.poc.potato.PotatoService;
import lombok.AllArgsConstructor;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@EnableBinding(PotatoChannel.class)
public class PotatoConsumer {

    private final PotatoService potatoService;

    @StreamListener(target = PotatoChannel.INPUT , condition = "headers['event-type']=='PotatoCreateEvent'")
    public void onReceiveMessage(@Payload PotatoCreateEvent potatoCreateEvent) {
        potatoService.save(Potato.of(potatoCreateEvent));
    }

}
