package br.com.jorge.poc.integration.potato;

import br.com.jorge.poc.core.DomainEvent;
import br.com.jorge.poc.core.exception.BusinessLogicException;
import br.com.jorge.poc.potato.Potato;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;

@Slf4j
@Getter
@ToString
@AllArgsConstructor
@Builder(toBuilder = true)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PotatoCreateEvent implements DomainEvent {

    private String potatoId;

    @ToString.Exclude
    private String description;

    @Override
    public String getEventType() {
        return this.getClass().getSimpleName();
    }

    public static PotatoCreateEvent of(Potato potato) {
        return PotatoCreateEvent.builder()
                .potatoId(potato.getUuid())
                .description(potato.getDescription())
                .build();
    }

    public Message<PotatoCreateEvent> message() {
        return MessageBuilder.withPayload(this)
                .setHeader("event-type", this.getEventType())
                .build();
    }

    public String toJson() {
        String json;
        try {
            json = new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new BusinessLogicException("Error to convert potato create event to json");
        }
        return json;
    }

    public static PotatoCreateEvent fromJson(String json) {
        PotatoCreateEvent potatoCreateEvent = null;
        try {
            potatoCreateEvent = new ObjectMapper().readValue(json, PotatoCreateEvent.class);
        } catch (JsonProcessingException e) {
            throw new BusinessLogicException("Error to convert potato create event from json");
        }
        return potatoCreateEvent;
    }
}
