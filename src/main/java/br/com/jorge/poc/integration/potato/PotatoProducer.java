package br.com.jorge.poc.integration.potato;

import br.com.jorge.poc.core.event.StoredDomainEvent;
import br.com.jorge.poc.core.event.StoredDomainEventService;
import lombok.AllArgsConstructor;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
@AllArgsConstructor
@EnableBinding(PotatoChannel.class)
public class PotatoProducer {

    private final PotatoChannel potatoChannel;
    private final StoredDomainEventService storedDomainEventService;
    private static final int DEFAULT_INIT_PAGE = 0;
    private static final int DEFAULT_ELEMENTS_SIZE = 2;

    @Scheduled(fixedRate = 5 * 1000)
    public void produceEvents() {
        Pageable pageable = PageRequest.of(DEFAULT_INIT_PAGE, DEFAULT_ELEMENTS_SIZE);
        Page<StoredDomainEvent> domainEvents = storedDomainEventService.findAllNotSentByType(pageable, PotatoCreateEvent.class.getSimpleName());
        domainEvents.forEach(this::produce);
    }

    @TransactionalEventListener(PotatoCreateEvent.class)
    public void produce(StoredDomainEvent domainEvent) {
        PotatoCreateEvent potatoCreateEvent = PotatoCreateEvent.fromJson(domainEvent.getData());
        try {
            potatoChannel.output().send(potatoCreateEvent.message());
            storedDomainEventService.sent(domainEvent);
        } catch (Exception e) {
            storedDomainEventService.notSent(domainEvent);
        }
    }

}
