package br.com.jorge.poc.core.event;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface StoredDomainEventService {
    StoredDomainEvent registryEvent(StoredDomainEvent storedDomainEvent);
    StoredDomainEvent sent(StoredDomainEvent storedDomainEvent);
    StoredDomainEvent notSent(StoredDomainEvent storedDomainEvent);
    Page<StoredDomainEvent> findAllNotSentByType(Pageable pageable, String type);
}
