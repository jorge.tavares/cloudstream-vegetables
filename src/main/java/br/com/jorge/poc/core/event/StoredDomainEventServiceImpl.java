package br.com.jorge.poc.core.event;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import static java.lang.Boolean.FALSE;

@Service
@AllArgsConstructor
class StoredDomainEventServiceImpl implements StoredDomainEventService {

    private final StoredDomainEventRepository storedDomainEventRepository;

    @Override
    public StoredDomainEvent registryEvent(StoredDomainEvent storedDomainEvent) {
        return storedDomainEventRepository.save(storedDomainEvent);
    }

    @Override
    public StoredDomainEvent sent(StoredDomainEvent storedDomainEvent) {
        return storedDomainEventRepository.save(storedDomainEvent.sent());
    }

    @Override
    public StoredDomainEvent notSent(StoredDomainEvent storedDomainEvent) {
        return storedDomainEventRepository.save(storedDomainEvent.notSent());
    }

    @Override
    public Page<StoredDomainEvent> findAllNotSentByType(Pageable pageable, String type) {
        return storedDomainEventRepository.findAllBySentAndType(pageable, FALSE, type);
    }
}
