package br.com.jorge.poc.core.event;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

interface StoredDomainEventRepository extends MongoRepository<StoredDomainEvent, String> {
    Page<StoredDomainEvent> findAllBySentAndType(Pageable pageable, Boolean sent, String type);
}
