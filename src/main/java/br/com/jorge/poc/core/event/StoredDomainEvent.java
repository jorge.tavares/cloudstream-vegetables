package br.com.jorge.poc.core.event;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.time.LocalDateTime;
import java.util.UUID;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Document(collection = "stored-domain-event")
public class StoredDomainEvent {

    @MongoId
    private String id;
    private String data;
    private String type;
    private LocalDateTime createdAt;
    private Boolean sent;

    public StoredDomainEvent sent() {
        return this.toBuilder()
                .sent(TRUE)
                .build();
    }

    public StoredDomainEvent notSent() {
        return this.toBuilder()
                .sent(FALSE)
                .build();
    }

    public static StoredDomainEvent createEvent(String data, String type) {
        return StoredDomainEvent.builder()
                .id(UUID.randomUUID().toString())
                .sent(Boolean.FALSE)
                .createdAt(LocalDateTime.now())
                .data(data)
                .type(type)
                .build();
    }

}
