package br.com.jorge.poc.potato;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PotatoService {
    Potato create(Potato potato);
    Potato save(Potato potato);
    Potato findById(String id);
    Page<Potato> findAll(Pageable pageable);
}
