package br.com.jorge.poc.potato.api;

import br.com.jorge.poc.potato.Potato;
import br.com.jorge.poc.potato.PotatoService;
import lombok.AllArgsConstructor;
import org.ff4j.FF4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static br.com.jorge.poc.configuration.Features.CREATE_POTATO_ASYNC;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.status;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/vegetables/potatos")
class PotatoRestService {

    private final PotatoService potatoService;
    private final FF4j ff4j;

    @PostMapping
    ResponseEntity<Potato> create(@RequestBody Potato potato) {
        ResponseEntity<Potato> response;
        potato = potatoService.create(potato);
        if (ff4j.check(CREATE_POTATO_ASYNC.name())) {
            response = noContent().build();
        } else {
            response = status(CREATED).body(potato);
        }
        return response;
    }

    @GetMapping("/{id}")
    @ResponseStatus(OK)
    Potato getById(@PathVariable String id) {
        return potatoService.findById(id);
    }

    @GetMapping
    @ResponseStatus(OK)
    Page<Potato> getAll(@RequestParam(defaultValue = "0", required = false) Integer page,
                        @RequestParam(defaultValue = "20", required = false) Integer size) {
        return potatoService.findAll(PageRequest.of(page, size));
    }
}
