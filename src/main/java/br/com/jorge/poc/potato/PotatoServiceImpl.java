package br.com.jorge.poc.potato;

import br.com.jorge.poc.core.event.StoredDomainEventService;
import br.com.jorge.poc.core.exception.ConflictException;
import br.com.jorge.poc.core.exception.ResourceNotFoundException;
import br.com.jorge.poc.integration.potato.PotatoCreateEvent;
import lombok.AllArgsConstructor;
import org.ff4j.FF4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static br.com.jorge.poc.configuration.Features.CREATE_POTATO_ASYNC;
import static br.com.jorge.poc.core.event.StoredDomainEvent.createEvent;

@Service
@AllArgsConstructor
class PotatoServiceImpl implements PotatoService {

    private final PotatoRepository potatoRepository;
    private final StoredDomainEventService storedDomainEventService;
    private final FF4j ff4j;

    @Override
    @Transactional
    public Potato create(Potato potato) {
        if (ff4j.check(CREATE_POTATO_ASYNC.name())) {
            PotatoCreateEvent potatoCreateEvent = PotatoCreateEvent.of(potato);
            storedDomainEventService.registryEvent(createEvent(potatoCreateEvent.toJson(), potatoCreateEvent.getEventType()));
        } else {
            potato = save(potato);
        }
        return potato;
    }

    @Override
    public Potato save(Potato potato) {
        potatoRepository.findById(potato.getUuid()).ifPresent(p -> { throw new ConflictException("Potato already exists"); });
        return potatoRepository.save(potato);
    }

    @Override
    public Potato findById(String id) {
        return potatoRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Potato not found for " + id));
    }

    @Override
    public Page<Potato> findAll(Pageable pageable) {
        return potatoRepository.findAll(pageable);
    }

}
