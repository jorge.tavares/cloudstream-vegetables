package br.com.jorge.poc.templates;

public enum FixtureTemplate {
    VALID,
    NOT_SENT,
    SENT
}
