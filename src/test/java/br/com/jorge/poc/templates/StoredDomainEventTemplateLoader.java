package br.com.jorge.poc.templates;

import br.com.jorge.poc.core.event.StoredDomainEvent;
import br.com.jorge.poc.integration.potato.PotatoCreateEvent;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.UUID;

import static br.com.jorge.poc.templates.FixtureTemplate.*;

public class StoredDomainEventTemplateLoader implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(StoredDomainEvent.class).addTemplate(VALID.name(), new Rule() {{
            add("id", UUID.fromString("ccc0d564-713f-4114-8d2d-3da8bceae7dc").toString());
            add("createdAt", LocalDateTime.of(2020, Month.JUNE, 21, 0, 0));
            add("type", "PotatoCreateEvent");
            add("data", "{\"potatoId\":\"a8334dd8-980a-4b1f-bfa7-acf8efd5f6f7\",\"description\":\"Im a boring potato\"}");
        }});

        Fixture.of(StoredDomainEvent.class).addTemplate(NOT_SENT.name()).inherits(VALID.name(), new Rule(){{
            add("sent", Boolean.FALSE);
        }});

        Fixture.of(StoredDomainEvent.class).addTemplate(SENT.name()).inherits(VALID.name(), new Rule(){{
            add("sent", Boolean.TRUE);
        }});
    }
}
