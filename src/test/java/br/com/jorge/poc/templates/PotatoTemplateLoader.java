package br.com.jorge.poc.templates;

import br.com.jorge.poc.potato.Potato;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

import java.util.UUID;

import static br.com.jorge.poc.templates.FixtureTemplate.VALID;

public class PotatoTemplateLoader implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(Potato.class).addTemplate(VALID.name(), new Rule() {{
            add("uuid", UUID.fromString("11a2c1e2-bb75-47df-8e8d-a2f24548777d").toString());
            add("description", "Im a happy potato o/");
        }});
    }
}
