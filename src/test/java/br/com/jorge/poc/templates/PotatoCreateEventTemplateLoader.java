package br.com.jorge.poc.templates;

import br.com.jorge.poc.integration.potato.PotatoCreateEvent;
import br.com.jorge.poc.potato.Potato;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

import java.util.UUID;

import static br.com.jorge.poc.templates.FixtureTemplate.VALID;

public class PotatoCreateEventTemplateLoader implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(PotatoCreateEvent.class).addTemplate(VALID.name(), new Rule() {{
            add("potatoId", UUID.fromString("a8334dd8-980a-4b1f-bfa7-acf8efd5f6f7").toString());
            add("description", "Im a boring potato ¬¬");
        }});
    }
}
