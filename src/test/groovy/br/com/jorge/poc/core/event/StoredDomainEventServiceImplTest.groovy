package br.com.jorge.poc.core.event

import br.com.jorge.poc.AbstractTestSpecification
import br.com.six2six.fixturefactory.Fixture
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import spock.lang.Shared

import java.time.LocalDateTime
import java.time.Month

import static br.com.jorge.poc.templates.FixtureTemplate.NOT_SENT

class StoredDomainEventServiceImplTest extends AbstractTestSpecification {

    @Shared
    StoredDomainEventRepository storedDomainEventRepository

    def setup() {
        storedDomainEventRepository = Mock(StoredDomainEventRepository)
    }

    def 'should registry a stored domain event with success' () {
        given:
        StoredDomainEventService storedDomainEventService = new StoredDomainEventServiceImpl(storedDomainEventRepository)
        StoredDomainEvent storedDomainEvent = Fixture.from(StoredDomainEvent.class).gimme(NOT_SENT.name())

        when:
        StoredDomainEvent event = storedDomainEventService.registryEvent(storedDomainEvent)

        then:
        1 * storedDomainEventRepository.save(storedDomainEvent) >> storedDomainEvent

        and:
        event.id == 'ccc0d564-713f-4114-8d2d-3da8bceae7dc'
        event.sent == Boolean.FALSE
        event.createdAt == LocalDateTime.of(2020, Month.JUNE, 21, 0, 0)
        event.data == '{"potatoId":"a8334dd8-980a-4b1f-bfa7-acf8efd5f6f7","description":"Im a boring potato"}'
        event.type == 'PotatoCreateEvent'
    }

    def 'should mark a stored domain event as sent' () {
        given:
        StoredDomainEventService storedDomainEventService = new StoredDomainEventServiceImpl(storedDomainEventRepository)
        StoredDomainEvent storedDomainEvent = Fixture.from(StoredDomainEvent.class).gimme(NOT_SENT.name())

        when:
        StoredDomainEvent sentEvent = storedDomainEventService.sent(storedDomainEvent)

        then:
        1 * storedDomainEventRepository.save(_ as StoredDomainEvent) >> storedDomainEvent.sent()

        and:
        sentEvent.sent == Boolean.TRUE
    }

    def 'should mark a stored domain event as not sent' () {
        given:
        StoredDomainEventService storedDomainEventService = new StoredDomainEventServiceImpl(storedDomainEventRepository)
        StoredDomainEvent storedDomainEvent = Fixture.from(StoredDomainEvent.class).gimme(NOT_SENT.name())

        when:
        StoredDomainEvent sentEvent = storedDomainEventService.notSent(storedDomainEvent)

        then:
        1 * storedDomainEventRepository.save(_ as StoredDomainEvent) >> storedDomainEvent.notSent()

        and:
        sentEvent.sent == Boolean.FALSE
    }

    def 'should retrieve all events not sent' () {
        given:
        StoredDomainEventService storedDomainEventService = new StoredDomainEventServiceImpl(storedDomainEventRepository)
        StoredDomainEvent storedDomainEvent = Fixture.from(StoredDomainEvent.class).gimme(NOT_SENT.name())
        Pageable firstPage = PageRequest.of(0, 1)

        when:
        Page<StoredDomainEvent> result = storedDomainEventService.findAllNotSentByType(firstPage, 'PotatoCreateEvent')

        then:
        1 * storedDomainEventRepository.findAllBySentAndType(firstPage, Boolean.FALSE, 'PotatoCreateEvent') >>
                new PageImpl<>(Arrays.asList(storedDomainEvent))

        and:
        result.totalElements == 1
        result.content.get(0).sent == Boolean.FALSE
    }
}
