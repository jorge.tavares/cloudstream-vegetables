package br.com.jorge.poc.integration.potato

import br.com.jorge.poc.AbstractTestSpecification
import br.com.jorge.poc.potato.Potato
import br.com.jorge.poc.potato.PotatoService
import br.com.six2six.fixturefactory.Fixture
import spock.lang.Shared

import static br.com.jorge.poc.templates.FixtureTemplate.VALID

class PotatoConsumerTest extends AbstractTestSpecification {

    @Shared
    PotatoService potatoService

    def setup() {
        potatoService = Mock(PotatoService)
    }

    def 'should consumes a potato created event with success' () {
        given:
        PotatoConsumer potatoConsumer = new PotatoConsumer(potatoService)
        PotatoCreateEvent potatoCreateEvent = Fixture.from(PotatoCreateEvent.class).gimme(VALID.name())

        when:
        potatoConsumer.onReceiveMessage(potatoCreateEvent)

        then:
        1 * potatoService.save(Potato.of(potatoCreateEvent)) >> Potato.of(potatoCreateEvent)
    }
}
