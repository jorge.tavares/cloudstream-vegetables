package br.com.jorge.poc.integration.potato

import br.com.jorge.poc.AbstractTestSpecification
import br.com.jorge.poc.core.event.StoredDomainEvent
import br.com.jorge.poc.core.event.StoredDomainEventService
import br.com.six2six.fixturefactory.Fixture
import org.junit.Ignore
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import spock.lang.Shared

import static br.com.jorge.poc.templates.FixtureTemplate.NOT_SENT

@Ignore
class PotatoProducerTest extends AbstractTestSpecification {

    @Shared
    PotatoChannel potatoChannel

    @Shared
    StoredDomainEventService storedDomainEventService

    def setup() {
        potatoChannel = Mock(PotatoChannel)
        storedDomainEventService = Mock(StoredDomainEventService)
    }

    def 'should produce a stored domain event with success' () {
        given:
        PotatoProducer potatoProducer = new PotatoProducer(potatoChannel, storedDomainEventService)
        List<StoredDomainEvent> storedDomainEvents = Fixture.from(StoredDomainEvent.class).gimme(5, NOT_SENT.name())
        Pageable firstPage = PageRequest.of(0, 2)

        when:
        potatoProducer.produceEvents()

        then:
        1 * storedDomainEventService.findAllNotSentByType(firstPage, PotatoCreateEvent.class.getSimpleName()) >>
                new PageImpl<StoredDomainEvent>(storedDomainEvents)
        1 * potatoChannel.output().send(_ as PotatoCreateEvent)
        1 * storedDomainEventService.sent(_ as PotatoCreateEvent)
    }

}
