package br.com.jorge.poc.potato.api

import br.com.jorge.poc.AbstractTestSpecification
import br.com.jorge.poc.potato.Potato
import br.com.jorge.poc.potato.PotatoService
import br.com.six2six.fixturefactory.Fixture
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader
import org.ff4j.FF4j
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpEntity
import spock.lang.Shared
import spock.lang.Specification

import static br.com.jorge.poc.configuration.Features.CREATE_POTATO_ASYNC
import static br.com.jorge.poc.templates.FixtureTemplate.VALID
import static java.lang.Boolean.FALSE
import static java.lang.Boolean.TRUE
import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.NO_CONTENT

class PotatoRestServiceTest extends AbstractTestSpecification {

    @Shared
    PotatoService potatoService

    @Shared
    FF4j ff4j

    def setup() {
        potatoService = Mock(PotatoService)
        ff4j = Mock(FF4j)
    }

    def 'should create a potato sync with success'() {
        given:
        PotatoRestService potatoRestService = new PotatoRestService(potatoService, ff4j)
        Potato potato = Fixture.from(Potato.class).gimme(VALID.name())

        when:
        HttpEntity<Potato> result = potatoRestService.create(potato)

        then:
        1 * potatoService.create(potato) >> potato
        1 * ff4j.check(CREATE_POTATO_ASYNC.name()) >> FALSE

        and:
        result.statusCode == CREATED
        result.body.uuid == '11a2c1e2-bb75-47df-8e8d-a2f24548777d'
        result.body.description == 'Im a happy potato o/'
    }

    def 'should create a potato async with success'() {
        given:
        PotatoRestService potatoRestService = new PotatoRestService(potatoService, ff4j)
        Potato potato = Fixture.from(Potato.class).gimme(VALID.name())

        when:
        HttpEntity<Potato> result = potatoRestService.create(potato)

        then:
        1 * potatoService.create(potato) >> potato
        1 * ff4j.check(CREATE_POTATO_ASYNC.name()) >> TRUE

        and:
        result.statusCode == NO_CONTENT
        result.body == null
    }

    def 'should get one potato by id with success'() {
        given:
        PotatoRestService potatoRestService = new PotatoRestService(potatoService, ff4j)
        Potato potato = Fixture.from(Potato.class).gimme(VALID.name())

        when:
        Potato result = potatoRestService.getById(potato.getUuid())

        then:
        1 * potatoService.findById(potato.getUuid()) >> potato

        and:
        result.uuid == potato.uuid
        result.description == potato.description
    }

    def 'should retrieve the potato first page with success' () {
        given:
        PotatoRestService potatoRestService = new PotatoRestService(potatoService, ff4j)
        Potato potato = Fixture.from(Potato.class).gimme(VALID.name())
        Pageable firstPage = PageRequest.of(0, 1)

        when:
        Page<Potato> result = potatoRestService.getAll(0, 1)

        then:
        1 * potatoService.findAll(firstPage) >> new PageImpl<>(Arrays.asList(potato))

        and:
        result.totalElements == 1
        result.content.get(0).uuid == potato.uuid
        result.content.get(0).description == potato.description
    }
}
