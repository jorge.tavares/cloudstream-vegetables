package br.com.jorge.poc.potato

import br.com.jorge.poc.AbstractTestSpecification
import br.com.jorge.poc.core.event.StoredDomainEvent
import br.com.jorge.poc.core.event.StoredDomainEventService
import br.com.jorge.poc.core.exception.ConflictException
import br.com.jorge.poc.core.exception.ResourceNotFoundException
import br.com.six2six.fixturefactory.Fixture
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader
import org.ff4j.FF4j
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import spock.lang.Shared
import spock.lang.Specification

import static br.com.jorge.poc.configuration.Features.CREATE_POTATO_ASYNC
import static br.com.jorge.poc.templates.FixtureTemplate.VALID

class PotatoServiceImplTest extends AbstractTestSpecification {

    @Shared
    PotatoRepository potatoRepository

    @Shared
    StoredDomainEventService storedDomainEventService

    @Shared
    FF4j ff4j

    def setup() {
        potatoRepository = Mock(PotatoRepository)
        storedDomainEventService = Mock(StoredDomainEventService)
        ff4j = Mock(FF4j)
    }

    def 'should create a not exists potato sync' () {
        given:
        PotatoService potatoGateway = new PotatoServiceImpl(potatoRepository, storedDomainEventService, ff4j)
        Potato potato = Fixture.from(Potato.class).gimme(VALID.name())

        when:
        Potato result = potatoGateway.create(potato)

        then:
        1 * ff4j.check(CREATE_POTATO_ASYNC.name()) >> Boolean.FALSE
        1 * potatoRepository.findById(potato.getUuid()) >> Optional.empty()
        1 * potatoRepository.save(potato) >> potato

        and:
        result.uuid == potato.uuid
        result.description == potato.description
    }

    def 'should thrown a conflict exception when tries to create a potato sync and already exists' () {
        given:
        PotatoService potatoGateway = new PotatoServiceImpl(potatoRepository, storedDomainEventService, ff4j)
        Potato potato = Fixture.from(Potato.class).gimme(VALID.name())

        when:
        potatoGateway.create(potato)

        then:
        1 * ff4j.check(CREATE_POTATO_ASYNC.name()) >> Boolean.FALSE
        1 * potatoRepository.findById(potato.getUuid()) >> Optional.of(potato)

        and:
        ConflictException e = thrown(ConflictException)

        and:
        e.message == 'Potato already exists'
    }

    def 'should create a potato async' () {
        given:
        PotatoService potatoGateway = new PotatoServiceImpl(potatoRepository, storedDomainEventService, ff4j)
        Potato potato = Fixture.from(Potato.class).gimme(VALID.name())

        when:
        Potato result = potatoGateway.create(potato)

        then:
        1 * ff4j.check(CREATE_POTATO_ASYNC.name()) >> Boolean.TRUE
        1 * storedDomainEventService.registryEvent(_ as StoredDomainEvent)

        and:
        result.uuid == potato.uuid
        result.description == potato.description
    }

    def 'should retrieve just one potato with success' () {
        given:
        PotatoService potatoGateway = new PotatoServiceImpl(potatoRepository, storedDomainEventService, ff4j)
        Potato potato = Fixture.from(Potato.class).gimme(VALID.name())

        when:
        Potato result = potatoGateway.findById(potato.getUuid())

        then:
        1 * potatoRepository.findById(potato.getUuid()) >> Optional.of(potato)

        and:
        result.uuid == potato.uuid
        result.description == potato.description
    }

    def 'should throw a resource not found exception when potato id not exists' () {
        given:
        PotatoService potatoGateway = new PotatoServiceImpl(potatoRepository, storedDomainEventService, ff4j)
        Potato potato = Fixture.from(Potato.class).gimme(VALID.name())

        when:
        potatoGateway.findById(potato.getUuid())

        then:
        1 * potatoRepository.findById(potato.getUuid()) >> Optional.empty()

        and:
        ResourceNotFoundException e = thrown(ResourceNotFoundException)

        and:
        e.message == 'Potato not found for ' + potato.getUuid()
    }


    def 'should retrieve a list of potato with success' () {
        given:
        PotatoService potatoGateway = new PotatoServiceImpl(potatoRepository, storedDomainEventService, ff4j)
        Potato potato = Fixture.from(Potato.class).gimme(VALID.name())
        Pageable firstPage = PageRequest.of(0, 1)

        when:
        Page<Potato> result = potatoGateway.findAll(firstPage)

        then:
        1 * potatoRepository.findAll(firstPage) >> new PageImpl<>(Arrays.asList(potato))

        and:
        result.totalElements == 1
        result.content.get(0).uuid == potato.uuid
        result.content.get(0).description == potato.description
    }

}
