FROM library/openjdk:11-slim
EXPOSE $PORT
MAINTAINER Jorge Tavares
COPY target/*.jar app.jar
ENTRYPOINT exec java -Xms724m -Xmx724m -jar $JAVA_OPTS /app.jar